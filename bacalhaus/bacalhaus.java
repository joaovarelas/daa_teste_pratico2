import java.util.*;


class bacalhaus{

    static public void main(String args[]){
	Scanner s = new Scanner(System.in);

	int n = s.nextInt(), r = s.nextInt();

	Grafo2 g = new Grafo2(n);
	
	while(r-->0){
	    int x = s.nextInt(), y = s.nextInt(),
		t = s.nextInt(), c = s.nextInt();
	    g.insert_new_arc(x,y,t,c);
	    g.insert_new_arc(y,x,t,c);
	}

	int k = s.nextInt();
	while(k != 0){
	    int max_temp = Integer.MIN_VALUE;
	    int min_temp = Integer.MAX_VALUE;
	    
	    int a = s.nextInt();
	    for(int i=0; i<k-1; i++){
		int b = s.nextInt();

		int temp = g.find_arc(a, b).valor0_arco();
		if(temp > max_temp) max_temp = temp;
		if(temp < min_temp) min_temp = temp;

		a = b;
	    }

	    System.out.printf("%d %d\n", min_temp, max_temp);
	    k = s.nextInt();
	}
    }
    
}
