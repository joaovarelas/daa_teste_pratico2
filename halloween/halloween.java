import java.util.*;


class halloween{

    static boolean[] bfs(Grafo0 g, int pos){

	int n = g.num_vertices();
	LinkedList<Integer> q = new LinkedList<Integer>();

	boolean[] visited = new boolean[n+1];
	Arrays.fill(visited, false);

	int root = pos;
	q.add(root);
	visited[root] = true;

	while(!q.isEmpty()){
	    int v = q.pop();
	    LinkedList<Arco> adjs = g.adjs_no(v);

	    for(Arco a : adjs){
		int w = a.extremo_final();

		if(!visited[w]){
		    visited[w] = true;
		    q.add(w);
		}
	    }
	    
	}

	return visited;	
    }

    
    

    public static void main(String args[]){
	Scanner s = new Scanner(System.in);

	int n = s.nextInt();
	int[] aboboras = new int[n+1];

	for(int i=1; i<=n; i++)
	    aboboras[i] = s.nextInt();

	int r = s.nextInt();

	Grafo0 g = new Grafo0(n);
	while(r-->0){
	    int x = s.nextInt(), y = s.nextInt();
	    g.insert_new_arc(x,y);
	    g.insert_new_arc(y,x);
	}

	int k = s.nextInt();

	while(k-->0){
	    //System.out.println("-----");
	    int pos = s.nextInt();

	    // caso supermercado tenha aboboras
	    if(aboboras[pos] != 0){
		System.out.println(pos);
		continue;
	    }
	  

	    /* */
	    boolean[] nodes = bfs(g, pos); // supermercados acessiveis



	    // supermercado sem aboboras e acessiveis tmb
	    boolean flag = false;
	    if(aboboras[pos] == 0){
		for(int i=1; i<=n; i++){
		    if(nodes[i]){
			if(aboboras[i] != 0){ flag = true; break; }
		    }

		}

		if(!flag){
		    System.out.println("Impossivel");
		    continue;
		}
	    }
	    



	    // pelo menos 1 acessivel c aboboras, escolhe o que tiver
	    // maior quantidade
	    int max = pos;
	    for(int i=n-1; i>=1; i--)
		if(nodes[i])
		    max = (aboboras[i] >= aboboras[max]) ? i : max;

	    System.out.println(max);
		    	    
	}

    }
}
