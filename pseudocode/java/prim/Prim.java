import java.util.*;

class Tuple{
    // x: parent node
    // y: child node
    int x; int y;
    Tuple(int x, int y){ this.x = x; this.y = y; }
}


class Prim{
    
    Set<Tuple> tree;

    
    Prim(Grafo g, int root){
	int n = g.num_vertices();

	int[] dist = new int[n+1];
	Arrays.fill(dist, Integer.MAX_VALUE);

	boolean[] visited = new boolean[n+1];
	Arrays.fill(visited, false);

	int[] parent = new int[n+1];
	Arrays.fill(parent, -1);
	
	dist[root] = 0;

	Heapmin q = new Heapmin(dist, n);
	tree = new HashSet<Tuple>();

	while(!q.isEmpty()){
	    int v = q.extractMin();

	    for(Arco a : g.adjs_no(v)){
		int w = a.extremo_final();
	     
		if(!visited[w] && a.valor_arco() < dist[w]){
		    dist[w] = a.valor_arco();
		    parent[w] = v;
		    q.decreaseKey(w, dist[w]);
		}
	    }
	    
	}
	
	   	
    }
	
}
