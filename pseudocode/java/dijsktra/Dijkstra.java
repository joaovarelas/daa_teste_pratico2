import java.util.*;

class Dijkstra{
    public int[] dist;
    
    Dijkstra(Grafo g, int root){
	int n = g.num_vertices();

	dist = new int[n+1];
	Arrays.fill(dist, Integer.MAX_VALUE);

	dist[root] = 0;

	Heapmin q = new Heapmin(dist, n);

	while(!q.isEmpty()){
	    int v = q.extractMin();

	    for(Arco a : g.adjs_no(v)){
		int w = a.extremo_final();
		int val = dist[v] + a.valor_arco();

		if(val < dist[w]){
		    dist[w] = val;
		    q.decreaseKey(w, dist[w]);
		}
		
	    }
	}
	
    }
}
