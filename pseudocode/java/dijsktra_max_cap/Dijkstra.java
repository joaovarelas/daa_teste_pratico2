import java.util.*;

class Dijkstra{
    public int[] cap;
    
    Dijkstra(Grafo g, int root){
	int n = g.num_vertices();

	cap = new int[n+1];
	Arrays.fill(cap, 0);

	cap[root] = Integer.MAX_VALUE;

	Heapmax q = new Heapmax(cap, n);

	while(!q.isEmpty()){
	    int v = q.extractMax();

	    for(Arco a : g.adjs_no(v)){
		int w = a.extremo_final();
		int val = Math.min(cap[v], a.valor_arco());

		if(val > cap[w]){
		    cap[w] = val;
		    q.increaseKey(w, cap[w]);
		}
		
	    }
	}
	
    }
}
