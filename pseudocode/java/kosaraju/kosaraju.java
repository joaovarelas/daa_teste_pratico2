import java.util.*;


class Kosaraju{

    public Stack<Integer> scc;
    Kosaraju(Grafo0 g, Grafo0 gt, int root){

        DFS d = new DFS(g, root);
	Stack<Integer> stack = d.stack;

	int n = gt.num_vertices();
	boolean[] visited = new boolean[n+1];

	while(!stack.isEmpty()){
	    int v = stack.pop();

	    scc = new Stack<Integer>();
	    scc.push(v);
	    
	    if(!visited[v])
		d.DFS_Visit(gt, v, visited, scc);


	    // ... scc contains strongly conn. comp. ...
	}

    }
    
}
