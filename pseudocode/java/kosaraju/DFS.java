import java.util.*;

class DFS{
    
    public Stack<Integer> stack;
    
    DFS(Grafo0 g, int root){

	int n = g.num_vertices();

	this.stack = new Stack<Integer>();
		
	boolean[] visited = new boolean[n+1];
	Arrays.fill(visited, false);

	for(int v=1; v<=n; v++)
	    if(!visited[v])
		DFS_Visit(g, v, visited, stack);

       	
    }


    void DFS_Visit(Grafo0 g, int v, boolean[] visited,
		   Stack<Integer> stack){
	
	visited[v] = true;

	for(Arco a : g.adjs_no(v)){
	    int w = a.extremo_final();
	    if(!visited[w])
		DFS_Visit(g, w, visited, stack);
		
	}
	
	stack.push(v);
    }

}
