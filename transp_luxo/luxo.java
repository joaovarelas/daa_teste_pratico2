import java.util.*;

class luxo{

    /*
    static int[] dijkstra(Grafo2 g, int src){
	int n = g.num_vertices();

	int[] dist = new int[n+1];
	Arrays.fill(dist, Integer.MAX_VALUE);

	dist[src] = 0;

	Heapmin q = new Heapmin(dist, n);

	while(!q.isEmpty()){
	    int v = q.extractMin();
	    for(Arco a : g.adjs_no(v)){
		int w = a.extremo_final();
		int custo = g.find_arc(v, w).valor1_arco();
		if(dist[v] + custo < dist[w]){
		    dist[w] = dist[v] + custo;
		    q.decreaseKey(w, dist[w]);
		}
	    }
	}

	return dist;
    }
    */

    

    static int[] minDist(Grafo2 g, int src, int dst){

	int n = g.num_vertices();
	
	boolean[] visited = new boolean[n+1];
	int[] dist = new int[n+1];

	Arrays.fill(visited, false);
	Arrays.fill(dist, Integer.MAX_VALUE);

	dist[src] = 0;
	
	LinkedList<Integer> q = new LinkedList<Integer>();		
	q.add(src);

	while(!q.isEmpty()){
	    int v = q.pop();
	    for(Arco a : g.adjs_no(v)){
		int w = a.extremo_final();
		if(!visited[w]){
		    visited[w] = true;
		    if(dist[v] + 1 < dist[w]) dist[w] = dist[v] + 1;
		    q.add(w);		    
		}
	    }
	}
		
	return dist;

    }

    
    public static void main(String args[]){
	Scanner s = new Scanner(System.in);

	int min_temp = s.nextInt(), max_temp = s.nextInt(),
	    src = s.nextInt(), dst = s.nextInt();

	int n = s.nextInt(), r = s.nextInt();

	Grafo2 g = new Grafo2(n);
	while(r-->0){
	    int x = s.nextInt(), y = s.nextInt(),
		t = s.nextInt(), c = s.nextInt();

	    if(t >= min_temp && t <= max_temp) {	
		g.insert_new_arc(x,y,t,c);
		g.insert_new_arc(y,x,t,c);
	    }
	    
	}


	int[] dist = minDist(g, src, dst);
        if(dist[dst] > n+1) System.out.println("Nao");
	else System.out.println("Sim "+dist[dst]);
	
    }
}


	  
