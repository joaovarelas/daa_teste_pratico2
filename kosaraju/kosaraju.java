import java.util.*;


class kosaraju{


    static Stack<Integer> DFS(Grafo0 g){
	int n = g.num_vertices();
	Stack<Integer> stack = new Stack<Integer>();
	boolean[] visited = new boolean[n+1];
	Arrays.fill(visited, false);

	for(int v=1; v<=n; v++){
	    if(!visited[v])
		DFS_Visit(g, v, visited, stack);
	}
	
       	return stack;
    }
    

    static Stack<Integer> sNodes;
    static void DFS_Visit(Grafo0 g, int v, boolean[] visited,
			  Stack<Integer> stack){
	visited[v] = true;

	
	sNodes.push(v);
	
	for(Arco a : g.adjs_no(v)){
	    int w = a.extremo_final();
	   
	    if(!visited[w])
		DFS_Visit(g, w, visited, stack);
	    
	}
	stack.push(v);
    }


    

    
    public static void main(String args[]){
	Scanner s = new Scanner(System.in);
	int nalunos = s.nextInt();

	int nodes = nalunos;
	Grafo0 g = new Grafo0(nodes);
	Grafo0 gt = new Grafo0(nodes);

	    
	while(nalunos-->0){
	    int aluno = s.nextInt();
	    int namigos = s.nextInt();
	    while(namigos-->0){
		int amigo = s.nextInt();
		g.insert_new_arc(aluno, amigo);
		gt.insert_new_arc(amigo, aluno); // G^t
	    }
	}


	
	
	// Algoritmo
	sNodes = new Stack<Integer>();
	Stack<Integer> gStack = DFS(g);
	Stack<Integer> gtStack = new Stack<Integer>();

	boolean[] visited = new boolean[nodes+1]; Arrays.fill(visited, false);


	
	while(!gStack.isEmpty()){
	    int v = gStack.pop();
	    sNodes = new Stack<Integer>();
 
	    if(!visited[v])
		DFS_Visit(gt, v, visited, gtStack);

	    int i = sNodes.size();
	    while(i-->0)
		System.out.print(sNodes.pop()+", ");
	    
	    System.out.println("\n----");
	}

	
		
    }
    
}
