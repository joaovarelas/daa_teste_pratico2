import java.util.*;

class otica{

    static int maxPrim(Grafo g, int max){
	int n = g.num_vertices();
	
	int[] dist = new int[n+1];
	boolean[] visited = new boolean[n+1];

	Arrays.fill(dist, -max);
	Arrays.fill(visited, false);

	int root = 1;
	dist[root] = 0;
	int weight = 0;
	
	Heapmax q = new Heapmax(dist, n);


	while(!q.isEmpty()){
	    int v = q.extractMax();
	    visited[v] = true;
	    weight += dist[v];

	    for(Arco a : g.adjs_no(v)){
		int w = a.extremo_final();

		if(!visited[w]){
		    
		    if(a.valor_arco() > dist[w]){
			dist[w] = a.valor_arco();
			q.increaseKey(w, dist[w]);
		    }
		    
		}
	    }

	    
	} // q Empty

	return weight;
    }

    
    static int DFS(Grafo g){
        int n = g.num_vertices();

	boolean[] visited = new boolean[n+1];
	Arrays.fill(visited, false);

	int root = 1;

	Stack<Integer> s = new Stack<Integer>();
	s.push(root);
	visited[root] = true;

	int connected = 1;

	while(!s.isEmpty()){
	    int v = s.pop();

	    for(Arco a : g.adjs_no(v)){
		int w = a.extremo_final();
		if(!visited[w]){
		    visited[w] = true;
		    connected++;
		    s.push(w);
		}
	    }

	} // stack empty

	return connected;
    }
    
    public static void main(String args[]){
	Scanner s = new Scanner(System.in);
	int n = s.nextInt(), r = s.nextInt(), c = s.nextInt();

	Grafo g = new Grafo(n);

	int max = Integer.MIN_VALUE;
	while(r-->0){
	    int x = s.nextInt(), y = s.nextInt(),
		b = s.nextInt();

	    g.insert_new_arc(x,y,b);
	    g.insert_new_arc(y,x,b);

	    if(b>max) max = b;
	}


	if(DFS(g) != n) System.out.println("impossivel");
	else {
	    int ans = maxPrim(g, max+1) - c*(n-1);
	    System.out.println("rendimento optimo: "+ans);
	}

	
    }
}
