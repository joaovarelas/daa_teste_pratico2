import java.util.*;


class Sociologia{


  
  
    static void DFS(Grafo0 g, int v, boolean visited[], Stack<Integer> stack) {  
        visited[v] = true; 
  
        LinkedList<Arco> adjs = g.adjs_no(v);
	
	for(Arco a : adjs){ 
            int n = a.extremo_final();
            if(!visited[n])
		DFS(g, n, visited, stack); 
        }
  
        stack.push(new Integer(v));
    } 
    

    static int total;
    static void DFS_Visit(Grafo0 g, int v, boolean visited[]) { 
        visited[v] = true; 

	total++;
	
	LinkedList<Arco> adjs = g.adjs_no(v);
            
        for(Arco a : adjs) { 
            int n = a.extremo_final();
            if (!visited[n]) 
                DFS_Visit(g, n, visited); 
        }
    } 


    static public void main(String args[]){
	Scanner s = new Scanner(System.in);

	int casos = s.nextInt();
	int caso = casos;

	while(casos-->0){
	    System.out.println("Caso #"+(caso-casos));
	    int nalunos = s.nextInt();

	    int nodes = nalunos;
	    Grafo0 g = new Grafo0(nodes);
	    Grafo0 gt = new Grafo0(nodes);

	    
	    while(nalunos-->0){
		int aluno = s.nextInt();
		int namigos = s.nextInt();
		while(namigos-->0){
		    int amigo = s.nextInt();
		    g.insert_new_arc(aluno, amigo);
		    gt.insert_new_arc(amigo, aluno); // G^t
		}

		
	    }


	    Stack<Integer> stack = new Stack<Integer>();
	    boolean[] visited = new boolean[nodes+1];
	    Arrays.fill(visited, false);

	    for(int i=1; i<=nodes; i++)
		if(!visited[i])
		    DFS(g, i, visited, stack);


	    int grupos4 = 0;
	    int pessoasfora = nodes;
	    
	    Arrays.fill(visited, false);
	    while (!stack.isEmpty()){
		total = 0;
		int v = (int)stack.pop();  
		if (!visited[v]){
		     DFS_Visit(gt, v, visited);
		     //System.out.println(total);
		     if(total >= 4){
			 grupos4++;
			 pessoasfora -= total;
		     }
		}
	    }

	    
	    System.out.printf("%d %d\n", grupos4, pessoasfora); 
	      
	}
	
	
    }
}
