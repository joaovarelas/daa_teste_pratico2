import java.util.*;



class rapido{

    static int bfs(Grafo g, int src, int dst){

	int n = g.num_vertices();
	LinkedList<Integer> q = new LinkedList<Integer>();
	
	boolean[] visited = new boolean[n+1];
	int[] dist = new int[n+1];
	
	Arrays.fill(visited,false);
	Arrays.fill(dist, Integer.MAX_VALUE);

	
	int root = src;

	dist[root] = 0;
	q.add(root);

	while(!q.isEmpty()){
	    int v = q.pop();
	    LinkedList<Arco> adjs = g.adjs_no(v);

	    for(Arco a : adjs){
		int w = a.extremo_final();
		if(!visited[w]){
		    visited[w] = true;
		    dist[w] = dist[v] + 1;
		    q.add(w);
		}
	    }
	    
	}

	//for(int i = 1; i <= n; i++)
	    //System.out.println(dist[i]);
	  
	return dist[dst];
	
    }
    

    public static void main(String args[]){
	Scanner s = new Scanner(System.in);
	int nodes = s.nextInt();

	int minWidth = s.nextInt(), maxWidth = s.nextInt(),
	    minLength = s.nextInt(), maxLength = s.nextInt(),
	    minHeight = s.nextInt();

	int src = s.nextInt(), dst = s.nextInt();

	int total = 0;
	
	Grafo g = new Grafo(nodes);

	
	int x = s.nextInt();
	
	while(x != -1){
	    int y = s.nextInt(),
		width = s.nextInt(), length = s.nextInt(),
		height = s.nextInt();
	    
	    if(width >= minWidth && length >= minLength && height >= minHeight){
		total++;
		g.insert_new_arc(x, y, length);
		g.insert_new_arc(y, x, length);				
	    }
	    
	    x = s.nextInt();
	}

	int trocos_min = bfs(g, src, dst);

	if(trocos_min == 0 || trocos_min > nodes+1) System.out.println("Impossivel");
	else System.out.println(trocos_min);
				   
      	
    }
}
