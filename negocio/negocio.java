import java.util.*;


class negocio{

    static int[] dijkstra(Grafo g, int src){
	int n = g.num_vertices();

	int[] dist = new int[n+1];
	Arrays.fill(dist, 2500);

	dist[src] = 0;

	Heapmin q = new Heapmin(dist, n);

        while(!q.isEmpty()){
	    int v = q.extractMin();
	    
	    for(Arco a : g.adjs_no(v)){
		int w = a.extremo_final();
		int custo = g.find_arc(v, w).valor_arco();	     
		if(dist[v] + custo < dist[w]){		    
		    dist[w] = dist[v] + custo;       
		    q.decreaseKey(w, dist[w]);		    
		}
	    }

	}

	return dist;
	
    }
    

    
    
    public static void main(String args[]){
	Scanner s = new Scanner(System.in);

	int n = s.nextInt(), dst = s.nextInt();

	Grafo g = new Grafo(n);

	
	int x = s.nextInt();
	while(x != -1){
	    int y = s.nextInt(), dist = s.nextInt();

	    g.insert_new_arc(x,y,dist);
	    g.insert_new_arc(y,x,dist);	    
	    
	    x = s.nextInt();
	}


	int[] dist = dijkstra(g, dst);

	for(int i=1; i<=n; i++){
	    int index = 1;
	    int min = dist[index];
	    
	    for(int j=2; j<=n; j++){
		if(dist[j] < min){
		    index = j;
		    min = dist[index];
		}
	    }

	    // Ghetto Fix Presentation Error
	    if(i==1) System.out.printf("%d", index);
	    else if(i==n) System.out.printf(" %d\n", index);
	    else System.out.printf(" %d", index);

	    
	    dist[index] = Integer.MAX_VALUE;
	    
	}
	
	
    }

}
