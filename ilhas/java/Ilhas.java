import java.util.*;


/*-------------------------------------------------------------------*\
  |  Definicao de GRAFOS SEM PESOS                                      |
  |     Assume-se que os vertices sao numerados de 1 a |V|.             |
  |                                                                     |
  |   A.P.Tomas, CC2001 (material para prova pratica), DCC-FCUP, 2017   |
  |   Last modified: 2017.12.18                                         |
  \--------------------------------------------------------------------*/

import java.util.LinkedList;

class Arco {
    int no_final;
    
    Arco(int fim){
	no_final = fim;
    }

    int extremo_final() {
	return no_final;
    }
}


class No {
    //int label;
    LinkedList<Arco> adjs;

    No() {
	adjs = new LinkedList<Arco>();
    }
}


class Grafo0 {
    No verts[];
    int nvs, narcos;
			
    public Grafo0(int n) {
	nvs = n;
	narcos = 0;
	verts  = new No[n+1];
	for (int i = 0 ; i <= n ; i++)
	    verts[i] = new No();
        // para vertices numerados de 1 a n (posicao 0 nao vai ser usada)
    }
    
    public int num_vertices(){
	return nvs;
    }

    public int num_arcos(){
	return narcos;
    }

    public LinkedList<Arco> adjs_no(int i) {
	return verts[i].adjs;
    }
    
    public void insert_new_arc(int i, int j){
	verts[i].adjs.addFirst(new Arco(j));
        narcos++;
    }

    public Arco find_arc(int i, int j){
	for (Arco adj: adjs_no(i))
	    if (adj.extremo_final() == j) return adj;
	return null;
    }
}



class Ilhas{

   


    public static void main(String[] args){
	Scanner s = new Scanner(System.in);

        Integer n = s.nextInt(), r = s.nextInt(); 

	Grafo0 g = new Grafo0(n);

	while(r-- > 0){
	    Integer x = s.nextInt(), y = s.nextInt();
	    g.insert_new_arc(x, y);
	    g.insert_new_arc(y, x);
	}


	Integer[] max_node = new Integer[n+1];
	for(int i=1; i<=n; i++) max_node[i] = i;

	
	for(int v=n; v>0; v--){
	    if(max_node[v] > v) continue;
	    
	    LinkedList<Integer> q = new LinkedList<Integer>();
	    q.add(v);

	    while(!q.isEmpty()){
		int node = q.pop();
		LinkedList<Arco> adjs = g.adjs_no(node);

		for(Arco a : adjs){
		    int w = a.extremo_final();
		
		    if(max_node[w] < v){
			q.add(w);
			max_node[w] = v;
		    }
		}
	    
	    }
	}

	Integer qx = s.nextInt();
	while(qx-->0){
	    int x = s.nextInt();
	    System.out.printf("No %d: %d\n", x, max_node[x]);
	}
	
    }
}
